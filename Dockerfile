FROM python:3.10
WORKDIR /pabd

COPY requirements.txt ./requirements.txt
RUN pip install -r requirements.txt

COPY ./src ./src
COPY ./models/model_1.joblib ./models/model_1.joblib

CMD ["python", "src/predict_app.py"]
EXPOSE 5000
