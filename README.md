# Analytics

House pricing prediction project. 
For CI/CD concepts demonstration.

## Installation
To install project

```commandline
git clone 
cd pabd
python -m venv venv
source venv/bin/activate
pip install -r requrements.txt
```

## Usage

Run prediction app

```commandline
python src/predict_app.py
```

## Todo

