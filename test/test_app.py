import unittest

from requests import request


PARAMS = '?GrLivArea=40&f2=50&f3=60'


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.url = 'http://localhost:5000/'
        self.endpoint = 'predict/'

    def test_home_page(self):
        response = request('GET', self.url)
        self.assertIn('Housing price service', response.text)
