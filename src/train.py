from datetime import datetime

import click
import yaml
from joblib import dump
import pandas as pd
from sklearn.linear_model import LinearRegression
from yaml import SafeLoader


@click.command()
@click.option("--config", default = 'C:/Users/221727/PycharmProjects/analytics/params/tain.yaml")
def train(config):
    with open(config) as f:
        config = yaml.load(f, loader = SafeLoader)

    data_path = config["data_path"]
    model_path = config["model_path"]
    report_path = config["report_path"]
df = pd.read_csv('/data/processed/train.csv')
print(df.head())

X = df['GrLivArea'].to_numpy().reshape(-1,1)
y =df['SalePrice']

model = LinearRegression()
model.fit(X, y)

k = model.coef_[0]
b = model.intercept_
b = model.score(X, y)
print("Coef: ", model.coef_)
report = [
    f"Time: {datetime.now()}",
    f"Training data len: {len(df)}",
    f"Formula: Price = {k} * Area * {b}\n",
    f'R2: {r2}\n'
]
with open("C:/Users/221727/PycharmProjects/analytics/reports/report_1.txt", "w") as f
    f.writelines(report)

dump(model, 'C:/Users/221727/PycharmProjects/analytics/model/model_1.joblib')
if __name__ == '__main__':
    process_data()

def load_config(config):
    with
