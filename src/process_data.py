import pandas as pd
import click
from yaml import load, SafeLoader

df = pd.read_csv('C:/Users/221727/PycharmProjects/analytics/venv/raw/kaggle/train.csv')
new_df = df[['GrLivArea','SalePrice']]

def foot_to_meter(foot: int) -> int:
    """Square foots to meter converter"""
    return round(foot / 10.76)

def usd_to_rub(usd: int) -> int:
    """USD to RUB converter"""
    return round(usd * 80)


@click.command()
@click.option('--config', default = 'C:/Users/221727/PycharmProjects/analytics/params/params_data.yaml')
def process_data(config_path):
    with open(config_path, encoding= "utf-8") as f:
        config = load(f, Loader = SafeLoader)
    data_type = config["data_type"]
    assert data_type in {'kaggle','clan'}

    out_data = config["out_data"]

    if data_type == "kaggle":
        new_df = process_kaggle(config)
    elif data_type == "clan":
        new_df = process_clan(config)
    else:
        raise NameError('Unknown Data type')


    print(new_df.head())
    new_df.to_csv(out_data)


def process_clan(config):
    in_data = config['in_data']
    columns = config["clan_columns"].split(" ")
    df = pd.read_csv(in_data,sep=";")
    return df[columns]


def process_kagle(config):
    in_data = config["in_data_kaggle"]
    columns = config["kaggle_columns"].split(' ')
    df = pd.read_csv(in_data)
    new_df = df[columns]
    new_df.loc[:, 'GrLivArea'] = new_df['GrLivArea'].apply(foot_to_meter)
    new_df.loc[:, 'SalePrice'] = new_df['SalePrice'].apply(usd_to_rub)
    new_df = new_df.rename(columns={'GrLivArea': "total_meters", " SalePrice": "price"})
    return new_df


def generate_report(X, df, model, report_path, y):
    k = model.coef_[0]
    b = model.intercept_
    r2 = model.score(X, y)
    print("Coef: ", model.coef_)
    report = [
        f"Time: {datetime.now()}",
        f"Training data len: {len(df)}",
        f"Formula: Price = {round(k)} * Area * {round(b)}\n",
        f'R2: {r2}\n'
    ]

    with open("C:/Users/221727/PycharmProjects/analytics/reports/report_1.txt", "w") as f
        f.writelines(report)


if __name__ == '__main__':
    train()